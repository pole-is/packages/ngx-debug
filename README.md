# @devatscience/ngx-debug
Source library to generate angular package ready for NPM

## How to
Once the changes are complete :
- Update the package.json file
- Increase version `"version": "0.1.1"`
- Run `npm build ngx-debug` to generate a **dist** folder that will contain all the required NPM files (typeScript & compiled es5 js)
- Move to the generated library **dist** folder `cd .../dist/ngx-debug`
- Check the package.json file
- Name should be `"name": "@devatscience/ngx-debug"`, version `"version": "0.1.1"`
- Run `npm publish --access public` to send it on npmjs

## Library features
- Dump panel
- Watcher
- Spy
- ApiRoute

## Peer dependencies
    "@devatscience/ngx-errors": "^0.1.0",
    "@angular/common": "^8.2.0",
    "@angular/core": "^8.2.0",
    "@angular/forms": "^8.2.0",
    "@angular/router": "^8.2.0",
    "lodash": "^4.17.15",
    "primeng": "^8.0.2",
    "rxjs": "^6.5.2",
    "rxjs-marbles": "^5.0.0",
