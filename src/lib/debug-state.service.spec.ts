import { marbles } from 'rxjs-marbles';

import { DebugStateService } from './debug-state.service';

describe('DebugStateService', () => {
  let debugStateService: DebugStateService;

  beforeEach(() => {
    debugStateService = new DebugStateService();
  });

  it('should construct', () => {
    expect(debugStateService).toBeDefined();
  });

  it('should ne enabled by default', () => {
    expect(debugStateService.enabled).toBeTruthy();
  });

  it('should be disabled by toggle', () => {
    debugStateService.toggle();
    expect(debugStateService.enabled).toBeFalsy();
  });

  it('should be enabled again', () => {
    debugStateService.toggle();
    debugStateService.toggle();
    expect(debugStateService.enabled).toBeTruthy();
  });

  it(
    'should emit state changed',
    marbles(m => {
      m.scheduler.schedule(
        () => debugStateService.toggle(),
        m.scheduler.createTime('-|')
      );

      m.scheduler.schedule(
        () => debugStateService.toggle(),
        m.scheduler.createTime('---|')
      );

      m.expect(debugStateService.enabled$).toBeObservable('tf-t', {
        f: false,
        t: true,
      });

      expect(debugStateService.enabled).toBeTruthy();
    })
  );
});
