import { Component, EventEmitter, Output } from '@angular/core';
import { tap } from 'rxjs/operators';

import { WatchedValue, WatchService } from './watch.service';

@Component({
  selector: 'dbg-watch-display',
  templateUrl: './watch-display.component.html',
})
export class WatchDisplayComponent {
  public readonly values$ = this.watchService.values$.pipe(
    tap(values => this.countChanged.emit(values.length))
  );

  @Output() public countChanged = new EventEmitter<number>();

  public constructor(private readonly watchService: WatchService) {}

  public label(_: number, value: WatchedValue): string {
    return value.label;
  }
}
