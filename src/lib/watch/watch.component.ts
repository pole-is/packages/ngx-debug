import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  BehaviorSubject,
  combineLatest,
  ReplaySubject,
  Unsubscribable,
} from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { WatchedValue, WatchService } from './watch.service';

@Component({
  selector: 'dbg-watch',
  template: '',
})
export class WatchComponent implements OnInit, OnDestroy {
  private readonly label$ = new BehaviorSubject('Watched value');
  private readonly value$ = new ReplaySubject<any>(1);
  private readonly dumpValue$ = combineLatest([this.label$, this.value$]).pipe(
    distinctUntilChanged(),
    map(([label, value]) => ({ label, value } as WatchedValue))
  );

  private subscription?: Unsubscribable;

  public constructor(private readonly watchService: WatchService) {}

  @Input() public set label(value: any) {
    this.label$.next(value);
  }

  @Input() public set value(value: any) {
    this.value$.next(value);
  }

  public ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      delete this.subscription;
    }
  }

  public ngOnInit(): void {
    if (!this.subscription) {
      this.subscription = this.watchService.register(this.dumpValue$);
    }
  }
}
