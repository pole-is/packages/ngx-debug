import { Injectable } from '@angular/core';
import { safeCombineLatest } from '@devatscience/ngx-rxtools';
import { BehaviorSubject, Observable, Unsubscribable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export interface WatchedValue {
  readonly label: string;
  readonly value: any;
}

@Injectable()
export class WatchService {
  private readonly registry = new Set<Observable<WatchedValue>>();
  private readonly registry$ = new BehaviorSubject(this.registry);

  public readonly values$ = this.registry$.pipe(
    switchMap(valueSet => safeCombineLatest(Array.from(valueSet)))
  );

  public register(value$: Observable<WatchedValue>): Unsubscribable {
    if (!this.registry.has(value$)) {
      this.registry.add(value$);
      this.registry$.next(this.registry);
    }
    return {
      unsubscribe: () => {
        if (this.registry.has(value$)) {
          this.registry.delete(value$);
          this.registry$.next(this.registry);
        }
      },
    };
  }
}
