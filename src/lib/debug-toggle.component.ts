import { Component } from '@angular/core';

import { DebugStateService } from './debug-state.service';

@Component({
  selector: 'dbg-debug-toggle',
  template: `
    <ng-container *ngIf="state.available">
      <p-toggleButton
        [ngModel]="state.enabled"
        (onChange)="state.toggle()"
        [offLabel]="null"
        [onLabel]="null"
        onIcon="fas fa-bug"
        offIcon="fas fa-bug"
      ></p-toggleButton>
    </ng-container>
  `,
})
export class DebugToggleComponent {
  public constructor(public readonly state: DebugStateService) {}
}
