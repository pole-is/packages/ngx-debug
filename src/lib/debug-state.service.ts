import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

export const enum DebugState {
  UNAVAILABLE = -1,
  DISABLED = 0,
  ENABLED = 1,
}

export const ProductionToken = new InjectionToken<boolean>('production');

@Injectable()
export class DebugStateService {
  private readonly state$: BehaviorSubject<DebugState>;

  public readonly enabled$: Observable<boolean>;

  public get state(): DebugState {
    return this.state$.getValue();
  }

  public get available(): boolean {
    return this.state !== DebugState.UNAVAILABLE;
  }

  public get enabled(): boolean {
    return this.state === DebugState.ENABLED;
  }

  public constructor(
    @Inject(ProductionToken) @Optional() production: boolean = false
  ) {
    this.state$ = new BehaviorSubject<DebugState>(
      production ? DebugState.UNAVAILABLE : DebugState.ENABLED
    );

    this.enabled$ = this.state$.pipe(
      map(state => state === DebugState.ENABLED),
      distinctUntilChanged()
    );
  }

  public toggle(): void {
    switch (this.state) {
      case DebugState.ENABLED:
        this.state$.next(DebugState.DISABLED);
        return;
      case DebugState.DISABLED:
        this.state$.next(DebugState.ENABLED);
        return;
    }
  }
}
