import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponseBase,
} from '@angular/common/http';
import * as _ from 'lodash';
import { Observable, ReplaySubject } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';

export interface RequestDebugEvent {
  id: string;
  type: 'request';
  request: HttpRequest<any>;
}

export interface ResponseDebugEvent {
  id: string;
  type: 'response';
  response: HttpResponseBase;
}

export interface FinalizeDebugEvent {
  id: string;
  type: 'finalize';
}

export type DebugEvent =
  | RequestDebugEvent
  | ResponseDebugEvent
  | FinalizeDebugEvent;

export class ApiDebugInterceptor implements HttpInterceptor {
  private readonly sink$ = new ReplaySubject<DebugEvent>(10);
  public readonly events$ = this.sink$.asObservable();
  private readonly enabled = true;

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!this.enabled) {
      return next.handle(request);
    }

    const id = _.uniqueId('req');
    this.emit({ type: 'request', id, request });

    return next.handle(request).pipe(
      tap({
        next: ev => this.sendResponse(id, ev),
        error: err => this.sendResponse(id, err),
      }),
      finalize(() => this.emit({ type: 'finalize', id }))
    );
  }

  private sendResponse(id: string, response: any): void {
    if (response instanceof HttpResponseBase) {
      this.emit({ type: 'response', id, response });
    }
  }

  private emit(ev: DebugEvent): void {
    this.sink$.next(ev);
  }
}
