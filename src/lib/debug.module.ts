import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LOCALE_ID, ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  ButtonModule,
  InputTextModule,
  OverlayPanelModule,
  PanelModule,
  SidebarModule,
  TabViewModule,
  ToggleButtonModule,
  TooltipModule,
} from 'primeng/primeng';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';

import { ApiBaseUrlToken, ApiDebugComponent } from './api/api-debug.component';
import { ApiDebugInterceptor } from './api/api-debug.interceptor';
import { DebugStateService, ProductionToken } from './debug-state.service';
import { DebugToggleComponent } from './debug-toggle.component';
import { DumpPanelComponent } from './dump-panel.component';
import { DumpValueComponent } from './dump-value.component';
import { SpyDisplayComponent } from './spy/spy-display.component';
import { WatchDisplayComponent } from './watch/watch-display.component';
import { WatchComponent } from './watch/watch.component';
import { WatchService } from './watch/watch.service';

const EXPORTED_COMPONENTS = [
  ApiDebugComponent,
  WatchComponent,
  DumpPanelComponent,
  DebugToggleComponent,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SidebarModule,
    ButtonModule,
    TreeModule,
    TreeTableModule,
    PanelModule,
    TabViewModule,
    ToggleButtonModule,
    InputTextModule,
    OverlayPanelModule,
    TooltipModule,
  ],
  providers: [
    WatchService,
    DebugStateService,
    { provide: LOCALE_ID, useValue: 'fr' },
  ],
  declarations: [
    //
    DumpValueComponent,
    WatchDisplayComponent,
    SpyDisplayComponent,
    ...EXPORTED_COMPONENTS,
  ],
  exports: [...EXPORTED_COMPONENTS],
})
export class DebugModule {
  public static forRoot(
    apiBaseUrl: string,
    production: boolean
  ): ModuleWithProviders {
    // TODO: fournir un module différent en prod (+ léger) et en dev (complet)
    return {
      ngModule: DebugModule,
      providers: [
        { provide: ProductionToken, useValue: production },
        { provide: ApiBaseUrlToken, useValue: apiBaseUrl },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ApiDebugInterceptor,
          multi: true,
        },
      ],
    };
  }
}
