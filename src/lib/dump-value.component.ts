import { Component, Input } from '@angular/core';
import { TreeNode } from 'primeng/api';

type PropertyPath = Array<string | number>;

@Component({
  selector: 'dbg-dump-value',
  templateUrl: './dump-value.component.html',
  styleUrls: ['./dump-value.component.scss'],
})
export class DumpValueComponent {
  private readonly expanded = new Set<string>();

  private _value: any = undefined;
  public root: TreeNode[] = [];
  public loading = true;

  @Input() public set value(value: any) {
    this.loading = false;
    if (value !== this._value) {
      this._value = value;
      this.root = [this.createNode(value)];
    }
  }

  public expand(path: PropertyPath) {
    this.expanded.add(path.join('§'));
  }

  public collapse(path: PropertyPath) {
    this.expanded.delete(path.join('§'));
  }

  public isExpanded(path: PropertyPath) {
    return this.expanded.has(path.join('§'));
  }

  private createNode(
    value: any,
    data: PropertyPath = [],
    refs = new Map<any, number>()
  ): TreeNode {
    if (value === null) {
      return { label: 'null', data, leaf: true, styleClass: 'null' };
    }
    if (value === undefined) {
      return { label: 'undefined', data, leaf: true, styleClass: 'undefined' };
    }
    if (value === false) {
      return { label: 'false', data, leaf: true, styleClass: 'boolean' };
    }
    if (value === true) {
      return { label: 'true', data, leaf: true, styleClass: 'boolean' };
    }

    if (typeof value === 'string') {
      return {
        label: JSON.stringify(value),
        data,
        leaf: true,
        styleClass: 'string',
      };
    }

    if (typeof value === 'number') {
      return {
        label: value.toString(),
        data,
        leaf: true,
        styleClass: 'number',
      };
    }

    const clazz = value.constructor.name;
    let ref = refs.get(value);
    if (ref) {
      return {
        label: `${clazz} @${ref}`,
        data,
        leaf: true,
        styleClass: 'reference',
      };
    }

    ref = refs.size;
    refs.set(value, ref);

    const entries = Object.entries(value);

    return {
      label: `${clazz} #${ref}`,
      data,
      leaf: entries.length === 0,
      children: entries
        .sort(([a], [b]) => (a < b ? -1 : a > b ? 1 : 0))
        .map(([key, item]) =>
          this.createNode(item, [].concat(data, [key]), refs)
        ),
      styleClass: 'object',
      expanded: this.isExpanded(data),
    };
  }
}
