import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { combineLatest, concat, of } from 'rxjs';
import { debounceTime, map, scan, share, tap } from 'rxjs/operators';

import { Notification, notifications$, NotificationType } from './spy.operator';

@Component({
  selector: 'dbg-spy-display',
  templateUrl: './spy-display.component.html',
  styleUrls: ['./spy-display.component.scss'],
})
export class SpyDisplayComponent {
  public filterForm = this.fb.group({
    orderByTag: false,
    eventTypes: this.fb.group({
      next: true,
      error: true,
      complete: true,
      subscribed: true,
      unsubscribed: true,
    }),
    match: '',
  });

  public readonly eventTypes: { [K in NotificationType]: string } = {
    next: 'paperclip',
    error: 'bomb',
    complete: 'stop',
    subscribed: 'bell',
    unsubscribed: 'bell-slash',
  };
  public readonly eventTypeList = Object.entries(this.eventTypes);

  @Output() public countChanged = new EventEmitter<number>();

  private readonly notifList$ = notifications$.pipe(
    tap(notif => this.consoleLog(notif)),
    scan((list: Notification[], n: Notification) => {
      list.push(n);
      return list;
    }, []),
    tap(notifs => this.countChanged.emit(notifs.length)),
    share()
  );

  public readonly notifs$ = combineLatest([
    this.notifList$,
    concat(
      of(this.filterForm.value),
      this.filterForm.valueChanges.pipe(
        debounceTime(300),
        share()
      )
    ),
  ]).pipe(
    map(([notifs, { match, eventTypes, orderByTag }]) => {
      const orderBy = orderByTag ? 'tag' : 'timestamp';

      let filter = (n: Notification) => eventTypes[n.type];
      if (match) {
        const origFilter = filter;
        filter = (n: Notification) => n.tag.includes(match) && origFilter(n);
      }

      return notifs.filter(filter).sort((a, b) => {
        const va = a[orderBy];
        const vb = b[orderBy];
        return va < vb ? -1 : va > vb ? 1 : 0;
      });
    })
  );

  public constructor(private readonly fb: FormBuilder) {}

  // tslint:disable:no-console
  private consoleLog(notif: Notification): void {
    const msg = `'tag=${notif.tag}; event=${notif.type}; timestamp=${notif.timestamp}`;
    if (notif.type === 'next' || notif.type === 'error') {
      console.groupCollapsed(msg);
      console.log(notif.payload);
      console.groupEnd();
    } else {
      console.log(msg);
    }
  }
}
