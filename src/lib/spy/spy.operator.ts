import { MonoTypeOperatorFunction, Observable, ReplaySubject } from 'rxjs';

export type NotificationType =
  | 'next'
  | 'error'
  | 'complete'
  | 'subscribed'
  | 'unsubscribed';

export interface Notification {
  tag: string;
  timestamp: number;
  type: NotificationType;
  payload?: any;
}

export const notifications$ = new ReplaySubject<Notification>(20);

const timeOrigin = Date.now();

export function spy<T>(tag = 'spy'): MonoTypeOperatorFunction<T> {
  function log(type: NotificationType, payload?: any) {
    notifications$.next({
      tag,
      type,
      payload,
      timestamp: Date.now() - timeOrigin,
    });
  }

  return (source$: Observable<T>): Observable<T> =>
    new Observable(observer => {
      log('subscribed');

      const sub = source$.subscribe({
        next: value => {
          log('next', value);
          observer.next(value);
        },
        error: err => {
          log('error', err);
          observer.error(err);
        },
        complete: () => log('complete'),
      });

      sub.add(() => log('unsubscribed'));

      return sub;
    });
}
