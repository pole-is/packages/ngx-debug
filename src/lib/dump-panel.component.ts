import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';

import { DebugStateService } from './debug-state.service';

@Component({
  selector: 'dbg-dump-panel',
  templateUrl: './dump-panel.component.html',
  styleUrls: ['./dump-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DumpPanelComponent {
  public visible = true;
  public fullScreen = false;

  public watchCount = 0;
  public spyCount = 0;
  public apiCallCount = 0;

  public constructor(
    public readonly debugState: DebugStateService,
    private readonly cd: ChangeDetectorRef
  ) {}

  public setWatchCount(count: number): void {
    this.watchCount = count;
    this.cd.markForCheck();
  }

  public setSpyCount(count: number): void {
    this.spyCount = count;
    this.cd.markForCheck();
  }

  public setApiCallCount(count: number): void {
    this.apiCallCount = count;
    this.cd.markForCheck();
  }
}
