/*
 * Public API Surface of ngx-debug
 */

export * from './lib/api/api-debug.interceptor';
export * from './lib/api/api-debug.component';
export * from './lib/debug.module';
export * from './lib/dump-value.component';
export * from './lib/watch/watch.component';
export * from './lib/watch/watch.service';
export * from './lib/debug-toggle.component';
export * from './lib/debug-state.service';
export * from './lib/spy/spy.operator';
